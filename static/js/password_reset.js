$(document).ready(function () {
    $("#resetPassForm").validate({
        rules: {
            password: {
                required: true,
                minlength: 6,
                maxlength: 12,
            },
            password2: {
                required: true,
                minlength: 6,
                maxlength: 12,
                equalTo: "#exampleInputPassword1",
            },
        },
    });
});