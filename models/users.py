import datetime

import jwt
from application import db, flask_bcrypt, redis_store
from models.base import Base
from passlib.hash import pbkdf2_sha256 as sha256
from config import key


class User(Base):
    """User Model for storing user related details."""
    __tablename__ = 'user'

    email = db.Column(db.String(255), unique=True, nullable=False)
    is_superuser = db.Column(db.Boolean, nullable=False, default=False)
    is_staff = db.Column(db.Boolean, nullable=False, default=False)
    username = db.Column(db.String(50), unique=True)
    password_hash = db.Column(db.String(100))
    password_reset_token = db.Column(db.String(255), nullable=True)
    last_login = db.Column(db.DateTime)
    is_active = db.Column(db.Boolean, default=True)
    jwt_reset_on = db.Column(db.DateTime, nullable=True)
    password_reset_on = db.Column(db.DateTime, nullable=True)

    profile = db.relationship(
        'Profile', backref="user", uselist=False, lazy=True
    )

    @property
    def password(self):
        raise AttributeError('password: write-only field')

    @password.setter
    def password(self, password):
        self.password_hash = sha256.hash(password)

    def check_password(self, password):
        return sha256.verify(password, self.password_hash)

    def __repr__(self):
        return F"<User '{self.username}'>"


class BlasklistToken(db.Model):
    """Toke Model for storing user token."""
    __tablename__ = 'blacklist_tokens'

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    token = db.Column(db.String(500), unique=True, nullable=False)
    blacklisted_on = db.Column(
        db.DateTime, nullable=False, default=datetime.datetime.utcnow())

    def __init__(self, token):
        self.token = token
        self.blacklisted_on = datetime.datetime.utcnow()

    def __repr__(self):
        return F'<id: token: {self.token}'

    @staticmethod
    def check_blacklist(auth_token):
        # check whether auth token has been blacklisted
        res = BlasklistToken.query.filter_by(token=str(auth_token)).first()
        if not res:
            return False
        return True


class UserType(Base):
    """Seed table of user types"""
    __tablename__ = 'user_type'

    """
    1. Admin
    2. Staff
    3. User
    """
    name = db.Column(db.String(30), nullable=True)


class Profile(Base):
    """User Profile table"""
    __tablename__ = 'profile'

    firstname = db.Column(db.String(100), nullable=True)
    lastname = db.Column(db.String(100), nullable=True)
    dob = db.Column(db.DateTime, nullable=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return F"<Profile '{self.firstname or self.user.username}'>"
