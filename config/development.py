from config.base import Base

class Config(Base):
    DEBUG = True
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root@localhost/flaskapp_development'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    REDIS_URL = "redis://0.0.0.0:6379/0"
    SECRET_KEY = "SOME_KEY"