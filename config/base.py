"""
Set Project level Configurations Setting in this file
"""
import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Base:
    SECRET_KEY = os.getenv('SECRET_KEY', 'MY_SECRET_KEY')
    DEBUG = False
