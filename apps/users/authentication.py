import datetime
import time
from flask import g
from application import redis_store
from helpers.jwt_helper import verify_jwt_token
from apps.users.blacklist_service import save_token
from models.users import User


def login(email, password):

    user = User.query.filter_by(email=email).first()
    if not user or not user.check_password(password):
        return None
    return user


def logout():
    if not g.token_obj.get("token", None):
        return False
    now = time.mktime(datetime.datetime.now().timetuple())
    auth_token = g.token_obj['token']
    redis_store.set(F"User_{auth_token}",
                    auth_token, int(g.token_obj["exp"]-now))
    return True


def get_logged_in_user(new_request):
    # get the auth token
    auth_token = new_request.headers.get('Authorization', None)
    if not auth_token:
        return None
    auth_token = auth_token.split(' ')[1]
    resp = verify_jwt_token(auth_token)
    print(resp, 'rrrr')
    if isinstance(resp, str):
        # Either token is exipred or invalid.
        return {"success": False, "message": resp}
    resp['token'] = auth_token
    return resp  # Token is valid
    # If token valid Ignoring Check user in database
    # try:
    #     return User.query.get(resp['id'])
    # except:
    #     return None
