import datetime
from flask import Blueprint, render_template, request
from helpers.helper import verify_password_reset_token
from models.users import User
from itsdangerous import BadSignature, SignatureExpired

user_blueprint = Blueprint(
    'user', __name__
)


@user_blueprint.route("/reset-password-view", methods=['GET', 'POST'])
def reset_password_view():
    if request.method == 'GET':
        password_reset_token = request.args.get('key', None)
        try:
            if not password_reset_token:
                raise Exception("Token not found")
            email = verify_password_reset_token(password_reset_token)
            user = User.query.filter_by(email=email).first()
            if not user or not user.password_reset_token == password_reset_token:
                raise Exception("Invalid token")
            context = {"email": user.email, "id": user.id,
                       "password_reset_token": password_reset_token}
            return render_template("password_reset.html", context=context)
        # except BadSignature as e:
        #     print(type(e).__name__, "1")
        #     return render_template("invalid_link.html")
        # except SignatureExpired as e:
        #     print(type(e).__name__, "2")
        #     return render_template("invalid_link.html")
        except Exception as e:
            print(type(e).__name__, "3")
            return render_template("invalid_link.html")

    else:
        try:
            request_data = request.form
            password_reset_token = request_data.get(
                'password_reset_token', None)
            user = User.query.filter_by(
                password_reset_token=password_reset_token).first()
            if not user:
                raise Exception("Invalid token")
            password = request_data.get('password', None)
            user.password = password
            user.password_reset_token = ''
            now = datetime.datetime.utcnow()
            user.password_reset_on = now
            user.jwt_reset_on = now
            user.save()
            return "Password has changed successfully."
        except Exception as e:
            print(e, "exception")
            return "Something went wrong please try again."
