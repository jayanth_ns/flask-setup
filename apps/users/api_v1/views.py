import datetime

from application import redis_store
from apps.users.authentication import login, logout
from apps.users.decorators import token_required
from apps.users.serializers import user_login_schema, user_schema, users_schema
from flask import (Blueprint, g, jsonify, request)
from models.users import User, Profile
from helpers.jwt_helper import generate_jwt_token
from helpers.helper import generate_password_reset_token

user_api_blueprint = Blueprint(
    'user_api', __name__,
)


@user_api_blueprint.route('/', methods=['GET', 'POST'])
def index():
    return jsonify({"success": True}), 200


@user_api_blueprint.route('/login', methods=['POST'])
def user_login():
    u = user_login_schema.load(request.get_json())
    if u.errors:
        return jsonify(u.errors), 401
    user = login(u.data.get('email'), u.data.get('password'))
    if not user:
        return jsonify({"message": "Invalid credentials"}), 400
    user.last_login = datetime.datetime.utcnow()
    user.save()
    payload = {
        'exp': datetime.datetime.utcnow() + datetime.timedelta(days=0, seconds=300),
        'iat': datetime.datetime.utcnow(),
        'sub': {'id': user.id, 'email': user.email, 'username': user.username}
    }
    auth_token = generate_jwt_token(payload)
    print(auth_token, 'ddddd')
    return jsonify({"message": "Successful login",
                    "auth_token": auth_token.decode("UTF-8"),
                    "succcess": True}), 200


@user_api_blueprint.route('/logout', methods=['POST'])
@token_required
def user_logout():
    auth_header = request.headers.get('Authorization')
    if logout():
        return jsonify({"message": "Successfully logged out",
                        "success": True}), 200
    return jsonify({"message": "Something went wrong. Please try again.",
                    "success": False}), 400


@user_api_blueprint.route('/new-user', methods=['POST'])
def register_new_user():
    u = user_schema.load(request.get_json())
    if u.errors:
        return jsonify({**u.errors, "success": False}), 401
    user_data = u.data
    user = User(
        email=user_data.get('email'),
        username=user_data.get('username')
    )
    user.password = user_data.get('password')
    user.save()
    Profile(user=user).save()
    return jsonify({"message": "New user registered successfully",
                    "success": True}), 201


@user_api_blueprint.route('/users', methods=['GET'])
@token_required
def all_users():
    # print(dir(request))
    print(request.host_url)
    users = User.query.all()
    result = users_schema.dump(users)
    return jsonify({"success": True, "users": result.data}), 200


@user_api_blueprint.route('/reset-password-request', methods=['GET'])
@token_required
def reset_password_request():
    user_id = g.user['id']
    user = User.query.get(user_id)
    user.password_reset_token = generate_password_reset_token(user.email)
    user.save()
    template_url = F'{request.host_url}user/reset-password-view?key={user.password_reset_token}'
    print(template_url, 'dddd')
    return jsonify({"success": True, "message": "Instructions are sent to your mail."})
