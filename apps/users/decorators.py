from functools import wraps

from apps.users.authentication import get_logged_in_user
from flask import jsonify, request, g


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        user = get_logged_in_user(request)
        if not user:
            return jsonify({"success": False, "message": "Token is not provided."}), 401
        if not user.get('success', True):
            return jsonify(user), 403
        g.user = user.pop('sub')
        g.token_obj = user
        return f(*args, **kwargs)

    return decorated
