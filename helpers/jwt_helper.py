import datetime

import jwt
from application import redis_store
from config import key


def generate_jwt_token(payload):
    """
    Generates the Auth Token
    :return: string
    """
    try:
        return jwt.encode(
            payload,
            key,
            algorithm='HS256'
        )
    except Exception as e:
        return e


def verify_jwt_token(token):
    """
    Decodes the auth token
    :param token:
    :return: dict
    """
    try:
        payload = jwt.decode(token, key)
        # is blacklisted token (checking in th redis)
        # code to check blacklisted token in redis
        is_blacklisted_token = redis_store.get(F"User_{token}")
        if is_blacklisted_token:
            raise Exception()
        return payload
    except jwt.ExpiredSignatureError:
        return "Token expired. Please log in again."
    except jwt.InvalidTokenError:
        return "Invalid token. Please log in again."
    except Exception as e:
        return "Invalid token. Please log in again."
