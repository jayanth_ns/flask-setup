from application import signer


def generate_password_reset_token(email):
    return signer.sign(email.encode("utf-8"))


def verify_password_reset_token(password_reset_token):
    # valid for 300 seconds
    # password_reset_token = password_reset_token.decode("utf-8")
    return signer.unsign(password_reset_token, max_age=10000)
