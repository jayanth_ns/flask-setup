from flask import Flask
from flask_bcrypt import Bcrypt
from flask_marshmallow import Marshmallow
from flask_redis import FlaskRedis
from flask_sqlalchemy import SQLAlchemy
from config import config_by_name, key
from itsdangerous import TimestampSigner


db = SQLAlchemy()
flask_bcrypt = Bcrypt()
ma = Marshmallow()
redis_store = FlaskRedis()
signer = TimestampSigner(key)


def create_app(config_env, register_blueprint=True):
    app = Flask(__name__)
    app.config.from_object(config_by_name[config_env])
    db.init_app(app)
    flask_bcrypt.init_app(app)
    ma.init_app(app)
    redis_store.init_app(app)

    if register_blueprint:
        from apps.index.views import index_blueprint
        app.register_blueprint(index_blueprint, url_prefix='/')

        from apps.users.api_v1.views import user_api_blueprint
        app.register_blueprint(user_api_blueprint, url_prefix='/api/user')

        from apps.users.views import user_blueprint
        app.register_blueprint(user_blueprint, url_prefix='/user')

        from apps.article.api_v1.views import article_api_blueprint
        app.register_blueprint(article_api_blueprint,
                               url_prefix='/api/articles')

    return app
